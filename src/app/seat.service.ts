import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class SeatService {
    constructor(private httpClient: HttpClient) {
    }
    getallseat(): Observable<any> {
        // Javascript object
        const request = { apiRequest: { action: 'list'}, 
        data: [{}]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/Seat', request);
    }
    getseat(): Observable<any> {
        // Javascript object
        const request = { apiRequest: { action: 'get'}, 
        data: [{id : '6e12694f-6dbc-4cd6-9013-be226d1c805b'}]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/Seat', request);
    }
    createseat(): Observable<any> {
        // Javascript object
        const request = { apiRequest: { action: 'new'}, 
        data: [{}]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/Seat', request);
    }
    updateseat(): Observable<any> {
        // Javascript object
        const request = { apiRequest: { action: 'update'}, 
        data: [{}]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/Seat', request);
    }
    removeeseat(): Observable<any> {
        // Javascript object
        const request = { apiRequest: { action: 'update'}, 
        data: [{}]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/Seat', request);
    }

}