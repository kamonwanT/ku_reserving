export interface ApiResponse {
    id: number;
    desc: string;
}
export interface CoreApiResponse<X> {
    apiResponse: ApiResponse;
    data: X[];
}
