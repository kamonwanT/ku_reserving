import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { ZonebComponent } from './admin/zoneb/zoneb.component';
import { ZonecComponent } from './admin/zonec/zonec.component';
import { ZonedComponent } from './admin/zoned/zoned.component';
import { ZoneeComponent } from './admin/zonee/zonee.component';
import { ZonefComponent } from './admin/zonef/zonef.component';
import { ZonegComponent } from './admin/zoneg/zoneg.component';
import { ZonepComponent } from './admin/zonep/zonep.component';
import { ZonesComponent } from './admin/zones/zones.component';
import { SetadminComponent } from './admin/setadmin/setadmin.component';
import { LoginuserComponent } from './admin/loginuser/loginuser.component';
import { LoginadminComponent } from './admin/loginadmin/loginadmin.component';
import { DatatableComponent } from './datatable/datatable.component';
const routes: Routes = [
  {path:'', redirectTo:'/admin', pathMatch: 'full'},
  {path:'home', component:HomeComponent},
  {path:'admin', component:AdminComponent},
  {path:'zoneb', component:ZonebComponent},
  {path:'zonec', component:ZonecComponent},
  {path:'zoned', component:ZonedComponent},
  {path:'zonee', component:ZoneeComponent},
  {path:'zonef', component:ZonefComponent},
  {path:'zoneg', component:ZonegComponent},
  {path:'zonep', component:ZonepComponent},
  {path:'zones', component:ZonesComponent},
  {path:'setadmin', component:SetadminComponent},
  {path:'loginuser', component:LoginuserComponent},
  {path:'loginadmin', component:LoginadminComponent},
  {path:'Datatable', component:DatatableComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
