import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,private modalService: NgbModal) { }
  period = [{
    id: 1, hour: ['1 period']
  },
  {
    id: 2, hour: ['2 period']
  },
  {
    id: 3, hour: ['3 period']
  }
  ];
  times = [{
    id: 1, time: ['8.30']
  },
  {
    id: 2, time: ['9.30']
  },
  {
    id: 3, time: ['10.30']
  }
  ];
  types = [{
    id: 1, type: ['Windows']
  },
  {
    id: 2, type: ['Mac']
  },
  {
    id: 3, type: ['Scanner']
  }
  ];
  ngOnInit() {
  }
  // popup
  open(content) {
    this.modalService.open(content, {size:"lg"});
  }
}
