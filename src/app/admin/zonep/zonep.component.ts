import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-zonep',
  templateUrl: './zonep.component.html',
  styleUrls: ['./zonep.component.css']
})
export class ZonepComponent implements OnInit {

  manageRoom: FormGroup;
  seatcom = {};
  closeResult: string;

  constructor(private router: Router, private modalService: NgbModal, private formBuilder: FormBuilder) { 
    const newcomputer = {
      category: '',
      ip: '',
      spec: '',
      zone: '',
      seat: ''
    }
    this.manageRoom = this.formBuilder.group(newcomputer)
  }

category = [{
  id: 1, type: ['Window']
},
{
  id: 2, type: ['Mac']
},
{
  id: 3, type: ['Scanner']
}
];

zonecom = [
  {
    id: 1, zone: 'A', seatcom: ['A1', 'A2', 'A3', 'A4', 'A5']
  },
  {
    id: 2, zone: 'B', seatcom: ['B1', 'B2', 'B3', 'B4', 'B5']
  },
  {
    id: 3, zone: 'C', seatcom: ['C1', 'C2', 'C3', 'C4', 'C5']
  },
  {
    id: 4, zone: 'D', seatcom: ['D1', 'D2', 'D3', 'D4', 'D5']
  },
  {
    id: 5, zone: 'E', seatcom: ['E1', 'E2', 'E3', 'E4', 'E5']
  },
  {
    id: 6, zone: 'F', seatcom: ['F1', 'F2', 'F3', 'F4', 'F5']
  },
  {
    id: 7, zone: 'G', seatcom: ['G1', 'G2', 'G3', 'G4', 'G5']
  },
  {
    id: 8, zone: 'P', seatcom: ['P1', 'P2']
  },
  {
    id: 9, zone: 'S', seatcom: ['S1', 'S2', 'S3', 'S4', 'S5']
  },
];

ngOnInit() {
  this.seatcom = this.zonecom.filter(x => x.id == 1)[0].seatcom;
}
onChange(deviceValue) {
  this.seatcom = this.zonecom.filter(x => x.id == deviceValue)[0].seatcom;
}

create() {
  console.log(this.manageRoom.value)
  alert('Success');
  this.router.navigate(['admin'])
}
  
// popup
  open(content) {
    this.modalService.open(content, {size:"lg"});
  }
  openedit(contents) {
    this.modalService.open(contents, {size:"lg"});
  }
  
  // page
  admin() {
    return this.router.navigate(['admin'])
  }
  zoneb() {
    return this.router.navigate(['zoneb'])
  }
  zonec() {
    return this.router.navigate(['zonec'])
  }
  zoned() {
    return this.router.navigate(['zoned'])
  }
  zonee() {
    return this.router.navigate(['zonee'])
  }
  zonef() {
    return this.router.navigate(['zonef'])
  }
  zoneg() {
    return this.router.navigate(['zoneg'])
  }
  zonep() {
    return this.router.navigate(['zonep'])
  }
  zones() {
    return this.router.navigate(['zones'])
  }

}
