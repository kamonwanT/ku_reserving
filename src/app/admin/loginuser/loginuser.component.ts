import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loginuser',
  templateUrl: './loginuser.component.html',
  styleUrls: ['./loginuser.component.css']
})
export class LoginuserComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  admin() {
    return this.router.navigate(['admin'])
  }
  setadmin() {
    return this.router.navigate(['setadmin'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
  loginadmin() {
    return this.router.navigate(['loginadmin'])
  }
}
