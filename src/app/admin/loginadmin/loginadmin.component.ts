import { Component, ViewChild, ElementRef, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup,FormBuilder } from '@angular/forms';
import { SeatService } from 'src/app/seat.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-loginadmin',
  templateUrl: './loginadmin.component.html',
  styleUrls: ['./loginadmin.component.css']
})
export class LoginadminComponent implements OnInit {
  seat : FormGroup
  zonedata:any

  @ViewChild('canvas') canvas: ElementRef;
  cx: CanvasRenderingContext2D;
  canvasEl: HTMLCanvasElement;
  ratio: number;
  x = 71.3;
  y = 96.5;
  x1 = 71.3;
  y1 = 93.5;
  x2 = 71.3;
  y2 = 90.8;
  x3 = 71.3;
  y3= 87.9;
  x4 = 71.4;
  y4 = 85.3;
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
      this.canvasEl.height = window.innerHeight;
      this.canvasEl.width = window.innerWidth;
      this.ratio = window.innerHeight / 100;
    // this.can
    // console.log(window.innerWidth);
  // this.innerWidth = window.innerWidth;
}

  constructor(private router: Router,private modalService: NgbModal,private FormBuilder: FormBuilder,private seatService:SeatService,private http: HttpClient) {
    this.seatService.getallseat().subscribe(result => {
      console.log(result);
      this.seat = result.data;
    });

   }
  ngOnInit() {
    this.canvasEl = this.canvas.nativeElement;
    this.cx = this.canvasEl.getContext('2d');
    this.onResize();

    this.zone()
  }
  draw() {
    // this.cx.fillStyle = '#000000';
    // this.cx.
    this.cx.beginPath();
    this.cx.arc(this.x * this.ratio, this.y * this.ratio, 5, 0, 2 * Math.PI);
    // this.cx.arc(this.x1 * this.ratio, this.y1 * this.ratio, 5, 0, 2 * Math.PI);
    // this.cx.arc(this.x2 * this.ratio, this.y2 * this.ratio, 5, 0, 2 * Math.PI);
    // this.cx.arc(this.x3 * this.ratio, this.y3 * this.ratio, 5, 0, 2 * Math.PI);
    // this.cx.arc(this.x4 * this.ratio, this.y4 * this.ratio, 5, 0, 2 * Math.PI);
    this.cx.fillStyle = 'red';
    this.cx.fill();
  }

  zone(){
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/Seat', data)
      .subscribe((res: any) => {
        this.zonedata = res.data
        console.log(res.data);
      })
  }























  // popup
  open(content) {
    this.modalService.open(content, {size:"lg"});
  }

  admin() {
    return this.router.navigate(['admin'])
  }
  setadmin() {
    return this.router.navigate(['setadmin'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
  loginadmin() {
    return this.router.navigate(['loginadmin'])
  }
  period = [{
    id: 1, hour: ['1 period']
  },
  {
    id: 2, hour: ['2 period']
  },
  {
    id: 3, hour: ['3 period']
  }
  ];
  times = [{
    id: 1, time: ['8.30']
  },
  {
    id: 2, time: ['9.30']
  },
  {
    id: 3, time: ['10.30']
  }
  ];
  // types = [{
  //   id: 1, type: ['Windows']
  // },
  // {
  //   id: 2, type: ['Mac']
  // },
  // {
  //   id: 3, type: ['Scanner']
  // }
  // ];
}
