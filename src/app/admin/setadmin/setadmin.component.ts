import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-setadmin',
  templateUrl: './setadmin.component.html',
  styleUrls: ['./setadmin.component.css']
})
export class SetadminComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  admin() {
    return this.router.navigate(['admin'])
  }
  setadmin() {
    return this.router.navigate(['setadmin'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
  loginadmin() {
    return this.router.navigate(['loginadmin'])
  }
  
}
