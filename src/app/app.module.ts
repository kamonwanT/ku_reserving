import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ZonebComponent } from './admin/zoneb/zoneb.component';
import { ZonecComponent } from './admin/zonec/zonec.component';
import { ZonedComponent } from './admin/zoned/zoned.component';
import { ZoneeComponent } from './admin/zonee/zonee.component';
import { ZonefComponent } from './admin/zonef/zonef.component';
import { ZonegComponent } from './admin/zoneg/zoneg.component';
import { ZonepComponent } from './admin/zonep/zonep.component';
import { ZonesComponent } from './admin/zones/zones.component';
import { SetadminComponent } from './admin/setadmin/setadmin.component';
import { LoginuserComponent } from './admin/loginuser/loginuser.component';
import { LoginadminComponent } from './admin/loginadmin/loginadmin.component';
import { DatatableComponent } from './datatable/datatable.component';
import { SeatService } from './seat.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminComponent,
    ZonebComponent,
    ZonecComponent,
    ZonedComponent,
    ZoneeComponent,
    ZonefComponent,
    ZonegComponent,
    ZonepComponent,
    ZonesComponent,
    SetadminComponent,
    LoginuserComponent,
    LoginadminComponent,
    DatatableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [SeatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
